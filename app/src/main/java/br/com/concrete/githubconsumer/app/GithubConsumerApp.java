package br.com.concrete.githubconsumer.app;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import br.com.concrete.githubconsumer.web.api.SSLCertificateHandler;

/**
 * Created by hdnn on 15/02/18.
 */

public class GithubConsumerApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // trust all SSL -> HTTPS connection
        SSLCertificateHandler.nuke();

        final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .build();

        ImageLoader.getInstance().init(config);
    }
}
