package br.com.concrete.githubconsumer.view.activity.impl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.concrete.githubconsumer.R;
import br.com.concrete.githubconsumer.model.PullRequest;
import br.com.concrete.githubconsumer.model.Repository;
import br.com.concrete.githubconsumer.view.activity.PullRequestsActivity;
import br.com.concrete.githubconsumer.view.component.adapter.PullRequestsAdapter;
import br.com.concrete.githubconsumer.web.api.GithubAPI;
import br.com.concrete.githubconsumer.web.api.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Response;

public class PullRequestsActivityImpl extends AppCompatActivity implements PullRequestsActivity {

    private Repository repository;

    private RecyclerView recyclerPR;

    private TextView emptyText;

    private PullRequestsAdapter adapter;

    private ProgressDialog dialog;

    private TextView openText;

    private TextView closedText;

    private TextView repoNameText;

    private TextView repoOwnerText;

    private int openCount = 0, closedCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerPR = findViewById(R.id.recycler_pull_requests);

        repository = (Repository) getIntent().getExtras().get("repository");

        emptyText = findViewById(R.id.txv_empty_view);

        openText = findViewById(R.id.txv_open_pr);

        closedText = findViewById(R.id.txv_closed_pr);

        repoNameText = findViewById(R.id.txv_repo_name);

        repoOwnerText = findViewById(R.id.txv_owner);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        recyclerPR.setLayoutManager(linearLayoutManager);

        adapter = new PullRequestsAdapter(this);

        recyclerPR.setAdapter(adapter);

        if (savedInstanceState != null) {
            adapter.setPullRequests(savedInstanceState.<PullRequest>getParcelableArrayList("prs"));
            openCount = savedInstanceState.getInt("openCount");
            closedCount = savedInstanceState.getInt("closedCount");
            repository = savedInstanceState.getParcelable("repo");
            openText.setText(String.format(getString(R.string.pr_open), openCount));
            closedText.setText(String.format(getString(R.string.pr_closed), closedCount));
        } else {
            loadPullRequests();
        }

        repoNameText.setText(repository.getName());
        repoOwnerText.setText(repository.getOwner().getLogin());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putParcelableArrayList("prs", (ArrayList<? extends Parcelable>) adapter.getPullRequests());
        savedInstanceState.putInt("openCount", openCount);
        savedInstanceState.putInt("closedCount", closedCount);
        savedInstanceState.putParcelable("repo", repository);


        super.onSaveInstanceState(savedInstanceState);
    }

    private void loadPullRequests() {
        dialog = ProgressDialog.show(this, "Aguarde",
                "Carregando pull requests...", true);

        final GithubAPI service = ServiceGenerator.createService(GithubAPI.class);
        final Call<List<PullRequest>> pullRequestsFromRepo = service.getPullRequestsFromRepo(repository.getOwner().getLogin(), repository.getName());
        pullRequestsFromRepo.enqueue(this);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onClickPullRequest(final PullRequest pullRequest) {
        final Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequest.getUrl()));
        startActivity(browserIntent);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {

        if (response.isSuccessful() && response.body() != null) {
            adapter.setPullRequests(response.body());

            openCount = 0;
            closedCount = 0;
            for (final PullRequest pr : response.body()) {
                if ("open".equals(pr.getState())) {
                    openCount++;
                } else if ("closed".equals(pr.getState())) {
                    closedCount++;
                }
            }

            openText.setText(String.format(getString(R.string.pr_open), openCount));
            closedText.setText(String.format(getString(R.string.pr_closed), closedCount));
        }

        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onFailure(Call<List<PullRequest>> call, Throwable t) {
        Toast.makeText(this, "Falha ao recuperar Pull  Requests =(, verifique sua conexão!", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }
}
