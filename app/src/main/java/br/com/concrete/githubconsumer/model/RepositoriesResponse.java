package br.com.concrete.githubconsumer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by hdnn on 15/02/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoriesResponse {

    @JsonProperty(value = "total_count")
    private long totalCount;

    @JsonProperty(value = "incomplete_results")
    private boolean incompleteResults;

    @JsonProperty(value = "items")
    private List<Repository> repositoryList;

    public long getTotalCount() {
        return totalCount;
    }

    public boolean isIncompleteResults() {
        return incompleteResults;
    }

    public List<Repository> getRepositoryList() {
        return repositoryList;
    }
}
