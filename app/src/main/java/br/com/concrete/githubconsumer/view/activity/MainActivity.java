package br.com.concrete.githubconsumer.view.activity;

import android.content.Context;

import br.com.concrete.githubconsumer.model.RepositoriesResponse;
import br.com.concrete.githubconsumer.model.Repository;
import retrofit2.Callback;

/**
 * Created by hdnn on 15/02/18.
 */

public interface MainActivity extends Callback<RepositoriesResponse> {

    Context getContext();

    void onRepositoryClicked(Repository repository);

}
