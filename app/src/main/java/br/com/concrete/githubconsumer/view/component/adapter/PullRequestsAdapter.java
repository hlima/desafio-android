package br.com.concrete.githubconsumer.view.component.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import br.com.concrete.githubconsumer.R;
import br.com.concrete.githubconsumer.model.PullRequest;
import br.com.concrete.githubconsumer.view.activity.PullRequestsActivity;
import br.com.concrete.githubconsumer.web.api.DateUtils;

/**
 * Created by hdnn on 15/02/18.
 */

public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsAdapter.PRViewHolder> {

    private List<PullRequest> pullRequests = new ArrayList<>();

    private PullRequestsActivity mListener;


    public PullRequestsAdapter(PullRequestsActivity listener) {
        mListener = listener;
    }

    public List<PullRequest> getPullRequests() {
        return pullRequests;
    }

    @Override
    public PRViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pr_list_item, parent, false);
        return new PRViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PRViewHolder holder, int position) {
        holder.mItem = pullRequests.get(position);

        //set show mode.

        holder.pullRequestTitleText.setText(holder.mItem.getTitle());
        holder.pullRequestBodyText.setText(holder.mItem.getBody());
        holder.usernameText.setText(holder.mItem.getUser().getUsername());
        ImageLoader.getInstance().displayImage(holder.mItem.getUser().getAvatarUrl(), holder.userAvatarImage);

        holder.dateText.setText(DateUtils.formatDate(holder.mItem.getCreatedAt()));

        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickPullRequest(holder.mItem);
            }
        });
    }


    public void setPullRequests(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    static class PRViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView usernameText;
        TextView pullRequestTitleText;
        TextView pullRequestBodyText;

        ImageView userAvatarImage;

        TextView dateText;

        PullRequest mItem;


        PRViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.card_view);
            usernameText = itemView.findViewById(R.id.txv_username);
            pullRequestTitleText = itemView.findViewById(R.id.txv_repo_name);
            pullRequestBodyText = itemView.findViewById(R.id.txv_repo_description);
            dateText = itemView.findViewById(R.id.txv_date);
            userAvatarImage = itemView.findViewById(R.id.imv_user_avatar);
        }

    }

}