/**
 * @author hildon.eduardo@gmail.com
 **/
package br.com.concrete.githubconsumer.view.activity.impl;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.concrete.githubconsumer.R;
import br.com.concrete.githubconsumer.model.APIError;
import br.com.concrete.githubconsumer.model.RepositoriesResponse;
import br.com.concrete.githubconsumer.model.Repository;
import br.com.concrete.githubconsumer.view.activity.MainActivity;
import br.com.concrete.githubconsumer.view.component.EndlessRecyclerViewScrollListener;
import br.com.concrete.githubconsumer.view.component.adapter.RepositoriesAdapter;
import br.com.concrete.githubconsumer.web.api.ErrorUtils;
import br.com.concrete.githubconsumer.web.api.GithubAPI;
import br.com.concrete.githubconsumer.web.api.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Response;

public class MainActivityImpl extends AppCompatActivity implements MainActivity {

    private RecyclerView recyclerRepositories;

    private EndlessRecyclerViewScrollListener scrollListener;

    private RepositoriesAdapter adapter;

    private int currentPage = 0;

    private TextView emptyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerRepositories = findViewById(R.id.recycler_repositories);
        emptyText = findViewById(R.id.txv_empty_view);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        recyclerRepositories.setLayoutManager(linearLayoutManager);
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                Log.d("Github Consumer", "Loading new page... " + page);
                loadNextDataFromApi(page + 1); //+1 to do not count initial state
            }
        };


        adapter = new RepositoriesAdapter(this);

        recyclerRepositories.addOnScrollListener(scrollListener);

        recyclerRepositories.setAdapter(adapter);

        if (savedInstanceState != null) {
            recyclerRepositories.setVerticalScrollbarPosition(savedInstanceState.getInt("position"));
            adapter.getRepositories().clear();
            adapter.appendRepositories(savedInstanceState.<Repository>getParcelableArrayList("repositories"));
            this.currentPage = savedInstanceState.getInt("currentPage");
            checkEmptyList();
        } else {
            loadNextDataFromApi(currentPage);
        }

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putInt("position", recyclerRepositories.getVerticalScrollbarPosition()); // get current recycle view position here.
        savedInstanceState.putParcelableArrayList("repositories", (ArrayList<? extends Parcelable>) adapter.getRepositories());
        savedInstanceState.putInt("currentPage", currentPage);


        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // Append the next page of data into the adapter
    // This method probably sends out a network request and appends new data items to your adapter.
    public void loadNextDataFromApi(final int page) {

        final GithubAPI apiService = ServiceGenerator.createService(GithubAPI.class);

        final Call<RepositoriesResponse> stars = apiService.listTopRepositories("language:Java", "stars", page);
        stars.enqueue(this);
    }

    @Override
    public void onResponse(Call<RepositoriesResponse> call, Response<RepositoriesResponse> response) {

        if (response.isSuccessful() && response.body() != null) {
            final RepositoriesResponse body = response.body();

            adapter.appendRepositories(body.getRepositoryList());
            currentPage++;

        } else {
            // parse the response body …
            final APIError error = ErrorUtils.parseError(response);
            // … and use it to show error information

            // … or just log the issue like we’re doing :)
            Log.d("error message", error.getMessage());
            Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();

        }
        checkEmptyList();
    }

    private void checkEmptyList() {
        if (adapter.getItemCount() == 0) {
            recyclerRepositories.setVisibility(View.GONE);
            emptyText.setVisibility(View.VISIBLE);
        } else {
            recyclerRepositories.setVisibility(View.VISIBLE);
            emptyText.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFailure(Call<RepositoriesResponse> call, Throwable t) {
        Toast.makeText(this, "Something went wrong! :(, check your connection", Toast.LENGTH_SHORT).show();
        Log.e("Github Consumer", "Error list repos: " + t.getLocalizedMessage());
        checkEmptyList();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onRepositoryClicked(final Repository repository) {
        final Intent intent = new Intent(this, PullRequestsActivityImpl.class);
        intent.putExtra("repository", repository);
        startActivity(intent);
    }
}
