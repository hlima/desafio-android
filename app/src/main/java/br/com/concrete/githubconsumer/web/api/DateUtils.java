package br.com.concrete.githubconsumer.web.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by hdnn on 15/02/18.
 */

public class DateUtils {

    public static String formatDate(final String dateUnformatted) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat dateFormatBr = new SimpleDateFormat("HH:mm 'de' dd/MM/yyyy");
        Date parse = null;
        try {
            parse = dateFormat.parse(dateUnformatted);
            String format = dateFormatBr.format(parse);
            return format;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
