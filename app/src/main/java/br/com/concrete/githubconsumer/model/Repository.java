package br.com.concrete.githubconsumer.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hdnn on 15/02/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Repository implements Parcelable {

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "full_name")
    private String fullName;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "owner")
    private Owner owner;

    @JsonProperty(value = "stargazers_count")
    private Integer stargazersCount;

    @JsonProperty(value = "forks_count")
    private Integer forksCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Integer getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(Integer stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public Integer getForksCount() {
        return forksCount;
    }

    public void setForksCount(Integer forksCount) {
        this.forksCount = forksCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.fullName);
        dest.writeString(this.description);
        dest.writeParcelable(this.owner, flags);
        dest.writeValue(this.stargazersCount);
        dest.writeValue(this.forksCount);
    }

    public Repository() {
    }

    protected Repository(Parcel in) {
        this.name = in.readString();
        this.fullName = in.readString();
        this.description = in.readString();
        this.owner = in.readParcelable(Owner.class.getClassLoader());
        this.stargazersCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.forksCount = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Repository> CREATOR = new Parcelable.Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel source) {
            return new Repository(source);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
}


//    Cada repositório deve exibir Nome do repositório, Descrição do Repositório, Nome / Foto do autor, Número de Stars, Número de Forks
