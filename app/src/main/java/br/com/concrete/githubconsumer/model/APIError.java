package br.com.concrete.githubconsumer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hdnn on 15/02/18.
 * <p>
 * Handling errors from github api
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class APIError {

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value = "document_url")
    private String documentUrl;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

}
