package br.com.concrete.githubconsumer.web.api;

import java.io.IOException;
import java.lang.annotation.Annotation;

import br.com.concrete.githubconsumer.model.APIError;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by hdnn on 15/02/18.
 */

public class ErrorUtils {

    public static APIError parseError(final Response<?> response) {
        final Converter<ResponseBody, APIError> converter =
                ServiceGenerator.retrofit.responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }

}
