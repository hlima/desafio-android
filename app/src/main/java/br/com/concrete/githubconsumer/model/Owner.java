package br.com.concrete.githubconsumer.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hdnn on 15/02/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Owner implements Parcelable {

    @JsonProperty(value = "id")
    private String id;

    @JsonProperty(value = "login")
    private String login;

    @JsonProperty(value = "avatar_url")
    private String avatarUrl;

    @JsonProperty(value = "html_url")
    private String htmlUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.login);
        dest.writeString(this.avatarUrl);
        dest.writeString(this.htmlUrl);
    }

    public Owner() {
    }

    protected Owner(Parcel in) {
        this.id = in.readString();
        this.login = in.readString();
        this.avatarUrl = in.readString();
        this.htmlUrl = in.readString();
    }

    public static final Parcelable.Creator<Owner> CREATOR = new Parcelable.Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel source) {
            return new Owner(source);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };
}
