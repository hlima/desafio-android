package br.com.concrete.githubconsumer.view.component.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import br.com.concrete.githubconsumer.R;
import br.com.concrete.githubconsumer.model.Repository;
import br.com.concrete.githubconsumer.view.activity.MainActivity;

/**
 * Created by hdnn on 15/02/18.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.RepositoriesViewHolder> {

    private List<Repository> repositories = new ArrayList<>();

    private MainActivity mListener;


    public RepositoriesAdapter(MainActivity listener) {
        mListener = listener;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    @Override
    public RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repository_list_item, parent, false);
        return new RepositoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RepositoriesViewHolder holder, int position) {
        holder.mItem = repositories.get(position);

        //set show mode.

        holder.repoNameText.setText(holder.mItem.getName());
        holder.repoDescriptionText.setText(holder.mItem.getDescription());
        holder.forkCountText.setText(String.valueOf(holder.mItem.getForksCount()));
        holder.starsCountText.setText(String.valueOf(holder.mItem.getStargazersCount()));
        holder.usernameText.setText(holder.mItem.getOwner().getLogin());
//        final Picasso picasso = Picasso.with(mListener.getContext());
//
//        picasso.load(holder.mItem.getOwner().getAvatarUrl())
//                .placeholder(R.drawable.ic_user_avatar).error(R.drawable.ic_user_avatar)
//                .into(holder.userAvatarImage);

        ImageLoader.getInstance().displayImage(holder.mItem.getOwner().getAvatarUrl(), holder.userAvatarImage);
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onRepositoryClicked(holder.mItem);
            }
        });
    }

    public void appendRepositories(List<Repository> repositories) {
        final int lastSize = repositories.size();
        this.repositories.addAll(repositories);
        notifyItemRangeInserted(lastSize, repositories.size());
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    static class RepositoriesViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView usernameText;
        TextView repoNameText;
        TextView repoDescriptionText;

        ImageView userAvatarImage;

        TextView forkCountText;
        TextView starsCountText;

        public Repository mItem;


        RepositoriesViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.card_view);
            usernameText = itemView.findViewById(R.id.txv_username);
            repoNameText = itemView.findViewById(R.id.txv_repo_name);
            repoDescriptionText = itemView.findViewById(R.id.txv_repo_description);
            forkCountText = itemView.findViewById(R.id.txv_fork_count);
            starsCountText = itemView.findViewById(R.id.txv_stars_count);
            userAvatarImage = itemView.findViewById(R.id.imv_user_avatar);
        }

    }

}