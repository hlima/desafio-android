package br.com.concrete.githubconsumer.web.api;

import java.util.List;

import br.com.concrete.githubconsumer.model.PullRequest;
import br.com.concrete.githubconsumer.model.RepositoriesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by hdnn on 15/02/18.
 */

public interface GithubAPI {


    @GET("search/repositories")
    Call<RepositoriesResponse> listTopRepositories(@Query("q") String query, @Query("sort") String sort, @Query("page") int page);

    @GET("repos/{creator}/{reponame}/pulls")
    Call<List<PullRequest>> getPullRequestsFromRepo(@Path("creator") String creator, @Path("reponame") String reponame);


}


