package br.com.concrete.githubconsumer.web.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by hdnn on 15/02/18.
 */

public class ServiceGenerator {

    private static OkHttpClient.Builder httpClient =
            new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE))
                    .connectTimeout(7000, TimeUnit.MILLISECONDS)
                    .readTimeout(7000, TimeUnit.MILLISECONDS)
                    .writeTimeout(7000, TimeUnit.MILLISECONDS);

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(JacksonConverterFactory.create());

    static Retrofit retrofit = builder.build();


    private static HttpLoggingInterceptor logging =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

    public static <S> S createService(
            Class<S> serviceClass) {

        if (!httpClient.interceptors().contains(logging)) {
            httpClient.addInterceptor(logging);
            builder.client(httpClient.build());
            retrofit = builder.build();
        }
        return retrofit.create(serviceClass);
    }
}