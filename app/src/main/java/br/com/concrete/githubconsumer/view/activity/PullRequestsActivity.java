package br.com.concrete.githubconsumer.view.activity;

import android.content.Context;

import java.util.List;

import br.com.concrete.githubconsumer.model.PullRequest;
import retrofit2.Callback;

/**
 * Created by hdnn on 15/02/18.
 */

public interface PullRequestsActivity extends Callback<List<PullRequest>> {

    Context getContext();

    void onClickPullRequest(PullRequest pullRequest);
}
