package br.com.concrete.githubconsumer;

import android.content.Intent;
import android.support.test.espresso.PerformException;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.util.HumanReadables;
import android.support.test.espresso.util.TreeIterables;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

import br.com.concrete.githubconsumer.model.Owner;
import br.com.concrete.githubconsumer.model.Repository;
import br.com.concrete.githubconsumer.view.activity.impl.PullRequestsActivityImpl;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by hdnn on 15/02/18.
 */

@RunWith(AndroidJUnit4.class)
public class PullRequestsActivityTest {
    @Rule
    public ActivityTestRule<PullRequestsActivityImpl>
            mActivityRule = new ActivityTestRule<>(PullRequestsActivityImpl.class, false, false);


    @Before
    public void setup() {
        Repository repository = new Repository();
        repository.setName("ReactiveX");
        Owner owner = new Owner();
        owner.setLogin("RxJava");
        repository.setOwner(owner);

        Intent i = new Intent();
        i.putExtra("repository", repository);
        mActivityRule.launchActivity(i);

    }

    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState() throws InterruptedException {

        onView(withText("Aguarde")).check(matches(isDisplayed()));//check if dialog loading is shown
        //progress dialog is now shown


    }
    /** Perform action of waiting for a specific view id. */
    public static ViewAction waitId(final int viewId, final long millis) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for a specific view with id <" + viewId + "> during " + millis + " millis.";
            }

            @Override
            public void perform(final UiController uiController, final View view) {
                uiController.loopMainThreadUntilIdle();
                final long startTime = System.currentTimeMillis();
                final long endTime = startTime + millis;
                final Matcher<View> viewMatcher = withId(viewId);

                do {
                    for (View child : TreeIterables.breadthFirstViewTraversal(view)) {
                        // found view with required ID
                        if (viewMatcher.matches(child)) {
                            return;
                        }
                    }

                    uiController.loopMainThreadForAtLeast(50);
                }
                while (System.currentTimeMillis() < endTime);

                // timeout happens
                throw new PerformException.Builder()
                        .withActionDescription(this.getDescription())
                        .withViewDescription(HumanReadables.describe(view))
                        .withCause(new TimeoutException())
                        .build();
            }
        };
    }
}
