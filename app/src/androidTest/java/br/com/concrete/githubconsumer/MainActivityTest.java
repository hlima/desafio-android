package br.com.concrete.githubconsumer;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.concrete.githubconsumer.view.activity.impl.MainActivityImpl;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by hdnn on 15/02/18.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivityImpl>
            mActivityRule = new ActivityTestRule<>(MainActivityImpl.class, false, true);


    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState() {
        onView(withId(R.id.recycler_repositories)).check(matches(isDisplayed()));
        onView(withId(R.id.textView)).check(matches(isDisplayed()));

    }
}
