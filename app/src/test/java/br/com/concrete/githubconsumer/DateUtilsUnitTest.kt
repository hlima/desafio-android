package br.com.concrete.githubconsumer

import br.com.concrete.githubconsumer.web.api.DateUtils
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class DateUtilsUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun formatDate_isCorrect() {
        assertEquals("07:34 de 09/02/2018", DateUtils.formatDate("2018-02-09T11:34:17Z"))
    }

    @Test
    fun formatDate_isIncorrect() {
        assertNotEquals("07:34 09/02/2018", DateUtils.formatDate("2018-02-09T11:34:17Z"))
    }

}
